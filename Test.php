<?php


use PHPUnit\Framework\TestCase;
require 'bizzbazz.php';

class Test extends TestCase
{
    function testsreturnsTheNumberPassedByParametersIfItIsNotAMultipleOf3or5(){
        $expected = "bizz bazz";
        $result = bizzbazz(15);
        self::assertEquals($expected,$result);
    }

    function testsreturnsBizzWhenTheNumberPassedByParametersIfMultipliOf3(){
        $expected = "bizz";
        $result = bizzbazz(3);
        self::assertEquals($expected,$result);
    }

    function testsreturns0WhenTheNumberPassedByParametersIf0(){
        $expected = 0;
        $result = bizzbazz(0);
        self::assertEquals($expected,$result);
    }

    function testsreturnsBizzWhenTheNumberPassedByParametersIfMultipliOf5(){
        $expected = "bazz";
        $result = bizzbazz(5);
        self::assertEquals($expected,$result);
    }

    function testsreturnsBizzWhenTheNumberPassedByParametersIfMultipliOf3and5(){
        $expected = "bizz bazz";
        $result = bizzbazz(15);
        self::assertEquals($expected,$result);
    }
}
