<?php

printNumber(16);

function printNumber($num)
{
    for ($i = 1; $i <= $num; $i++) {
        echo "Número: " . $i . " => " . bizzbazz($i) . "\n";
    }
}

function bizzbazz($num)
{
    $divisible = fn($divisor, $num) => ($num % $divisor === 0);

    if ($num == 0) {
        return 0;
    }
    if ($divisible(3, $num) && $divisible(5, $num)) {
        return "bizz bazz";
    }
    if ($divisible(3, $num)) {
        return "bizz";
    }
    if ($divisible(5, $num)) {
        return "bazz";
    }
    return $num;
}


